package app.currencies.adapter;

public interface OnCurrencyValueChanged {
    void onValueChanged(int position, int value);
}
