package app.currencies.adapter;

import android.support.v7.util.DiffUtil;

import java.util.List;

import app.currencies.model.Currency;

public class CurrencyDiffUtilCallback extends DiffUtil.Callback {

    private List<Currency> mOld;
    private List<Currency> mNew;

    public CurrencyDiffUtilCallback(List<Currency> oldList, List<Currency> newList) {
        mOld = oldList;
        mNew = newList;
    }

    @Override
    public int getOldListSize() {
        return mOld == null ? 0 : mOld.size();
    }

    @Override
    public int getNewListSize() {
        return mNew == null ? 0 : mNew.size();
    }

    @Override
    public boolean areItemsTheSame(int i, int i1) {
        Currency oldItem = mOld.get(i);
        Currency newItem = mNew.get(i1);

        return oldItem.getShortName().equals(newItem.getShortName());
    }

    @Override
    public boolean areContentsTheSame(int i, int i1) {
        Currency oldItem = mOld.get(i);
        Currency newItem = mNew.get(i1);

        return oldItem.getRate().equals(newItem.getRate());
    }
}
