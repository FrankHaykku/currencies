package app.currencies;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;

public class CurrencyTextWatcher implements TextWatcher {

    private String beforeText = "";

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        beforeText = charSequence.toString();
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if(charSequence == null)
            return;

        int initialCursorPos = i1 + i2;
        int countAfterCursorPos = getNumberOfDigits(beforeText.substring(initialCursorPos, beforeText.length()));


    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private int getNumberOfDigits(String text){
        int count = 0;
        for(int i = 0; i < text.length(); i++)
            if(Character.isDigit(text.charAt(i)))
                count++;

        return count;
    }
}
