package app.currencies.ui;

import app.currencies.R;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import app.currencies.adapter.CurrenciesListAdapter;
import app.currencies.adapter.OnClickListener;
import app.currencies.model.Currency;


public class CurrenciesRatesActivity extends MvpAppCompatActivity implements CurrenciesRatesView {

    @InjectPresenter
    CurrenciesRatesPresenter mPresenter;
    CurrenciesListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewInit();
    }

    private void viewInit() {
        RecyclerView rv = findViewById(R.id.currencies_rv);
        rv.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new CurrenciesListAdapter(null, listener);
        rv.setAdapter(mAdapter);
    }

    private OnClickListener listener = new OnClickListener() {
        @Override
        public void onClick(int position) {
            mPresenter.disposeRequest();
            onCurrencyValueClick(position);
        }

        @Override
        public void onValueChanged(int position, double value) {
            mPresenter.setRate(value);
            mPresenter.resumeRequest();
        }
    };

    private void onCurrencyValueClick(int position) {
        Currency currency = mAdapter.getItem(position);
        mPresenter.setCurrentCurrencyName(currency.getShortName());
        mAdapter.moveItemToHead(position);
    }

    @Override
    public void update(List<Currency> currencies) {
        if(mAdapter.getItems() == null)
            mAdapter.setData(currencies);
        else
            mAdapter.updateData(currencies);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.resumeRequest();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.disposeRequest();
    }
}
